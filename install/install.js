// Install process

// Deps
var fs = require('fs');
var sys = require('sys')
var exec = require('child_process').exec;

// Functions
function puts(error, stdout, stderr) { sys.puts(stdout) }

// Copy README.md
exec("cp README.md ../../", puts);

// Symlink config.json
exec("ln ../src/config.json ../../config.json", puts);

// Create our zork.sh
var stream = fs.createWriteStream("../../zork.sh");
stream.once('open', function(fd) {
  stream.write("cd node_modules/zork-bot/src/\n");
  stream.write("node main.js");
  stream.end();
});

// Make zork.sh executable
exec("chmod +x ../../zork.sh", puts);