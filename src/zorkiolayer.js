// zorkiolayer.js
//
// Now with no expect!
// Party on Bill! Party on Ted!


var dbg         = require('./debuglayer');
var fs          = require('fs');
var ink         = require('./colors');
var colors      = require('./colors').colors;
var spawn       = require('child_process').spawn;
var filt        = require('./filters.js');
var debug       = require('./debuglayer');
var config_data = require('./main').config_data;
var procmgr     = require('./proc_manager');
var users       = require('./users');
var mp          = require('./multiplayer');
//var io = require('./webserver').io;

var write_apache_style_log = require('./sysloglayer').write_apache_style_log;


var eh = require('./main').mainEmitter;

// EVENT: SEND TO ZORK
// zork layer received a command from some communication layer, send it to zork
//eh.on('zork_writePlayerCmd', function (nick, msg) {
exports.writePlayerCmd = function (nick, msg) {

  var sps = users.get();
  var child = sps[nick].child;

  mp.catchCmd(sps, nick, msg);

  console.log(ink.itag+' ZORK LAYER: '.info+nick+': got child object for player '.info+child);

  var ok = child.stdin.write(msg+"\r\n");
}


// EVENT: SPAWN ZORK
// SPAWN and all that
//eh.on('startzork', function (nick, version, version_dat_file) {
exports.startzork = function (nick, version, version_dat_file) {

  // TODO: Make sure game isnt already started by another module
  // couldve already been started by signal from irc or web

  console.log(ink.wtag+" ZORK LAYER: ".warn+nick+": player spawning game with: ".warn +nick.verbose+', '
    +version+', '+version_dat_file);

  var args = ['-p', version_dat_file];
  var sps = procmgr.spawnsub(nick, 'dfrotz', args);
  var child = sps[nick].child;

  // make sure this is initialized
  if (sps[nick].zork === undefined) {
    sps[nick].zork = {};
    sps[nick].zork.lastscene = "Start of game";
    sps[nick].zork.cmdcnt = 0;
    sps[nick].zork.usersInScene = {};
  }

  console.log(ink.itag+' ZORK LAYER: '.info+nick+': created process child object for player '.info);

  for(var key in sps) { // loop through all users and notify them a player has joined
     if(key != nick) 
        eh.emit('sendPlayerMsg', key, nick+" has joined the game.");
  }

  child.stdin.setEncoding('utf8');
  child.stdout.setEncoding('utf8');

  child.stdout.on('data', function (data) {
      console.log(ink.itag+' ZORK LAYER: '.info+nick+' got bytes of data:'+ data.length);

      cdata = data.toString();
      cdata = filt.cutFirstLine(cdata);
      cdata = filt.removeNewlines(cdata);

      mp.parseMultiplayerCmds(nick, cdata);
 
      // need to have a way to determin if this is going to an irc player or minecraft player
      if (nick.substring(0,3) != "mc-") {
        //eh.emit('sendPlayerMsg', nick, mp.doHealthReport(nick));
      	if((sps[nick] !== undefined) && (!sps[nick].zork.issyscmd))  {
          	
          	console.log(cdata);
          	if (cdata.substring(0,23) != "Please enter a filename" &&
          	    cdata.substring(0,25) != " >Please enter a filename") {

            	eh.emit('sendPlayerMsg', nick, cdata);
            	mp.doLocations(nick);
            	//io.emit('chat message', mp.doMpReport());
	        		//io.emit('stats message', mp.doStatsReport());
            }
        }

      } else {
      	
      		eh.emit('sendPlayerMCMsg', nick, cdata);
      	
      }
  });

  child.stderr.on('data', function (data) {
    console.log(ink.itag+' ZORK LAYER: error from child for player: '.info+nick+' stderr: '+data );
  });

  child.on('close', function (code) {
    console.log(ink.itag+' ZORK LAYER: child process for player: '.info+nick+' exited with code '.info + code);
  });  
  child.on('end', function () {
    console.log(ink.itag+' ZORK LAYER: child out of data'.error);
  });

  console.log(ink.itag+" ZORK LAYER: ".info+nick+": game spawned for player".info);

  return sps; // sps holds the list of users, return it in case any callers need it
}


// EVENT ENDZORK sent from comm layer
//eh.on('endzork', function (nick) {
exports.endzork = function (nick) {

  var sps = users.get();
  var child = sps[nick].child;
  for(var key in sps) {
    var val = sps[key];
    console.log(ink.itag+" ZORK LAYER: Zork processes before kill: Key: ".info+key+" value:"+val);
  }

  console.log(ink.itag+" ZORK LAYER: ".info+nick+": sending SIGTERM to process for: ".info);
  child.kill();  // send SIGTERM by default
  delete sps[nick];

  for(var key in sps) {
    var val = sps[key];
    console.log(ink.itag+" ZORK LAYER: Zork processes after kill: Key: ".info+key+" value:"+val);
  }

  console.log(ink.itag+' ZORK LAYER: '.info+nick+' killed child and deleted child object: '.info+child);

  for(var key in sps) { // loop through all users and notify them a player has joined
     eh.emit('sendPlayerMsg', key, nick+" has left the game.");
  }
}


// this controls if the nick will see both system and dummy commands
exports.setDebug = function(nick, bool) {

  var usrs = users.get();

  usrs[nick].debug = bool;
}
