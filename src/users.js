// user/player management 
// structures, getters, etc

var ink         = require('./colors');

var zork        = require('./zorkiolayer');

var fs = require('fs');
var exec = require('child_process').exec;
var sys = require('sys')

var users = {};

// EVENT HANDLING
// setup event handler eh
var eh = require('./main').mainEmitter;


/* users object structure
{
  fetter: {
    child:      child_process_object,
    nick:       "usernick",
    admin:      boolean,
    zork: {
      health:     "health data returned by zork",
      lastscene:  "lastscene data returned by zork",
      lastcmd:    "last cmd user sent to zork",
      usersInScene: {
        user1: "user1",
        fatal:  "fatal"
      }
    },
    irc: { // any irc specific data
    },
    mc: { // any mc specific data
    }
  },
  fatal: {
    child: child_process_object
    zork: {
      health:     "health data returned by zork",
      lastscene:  "lastscene data returned by zork",
      lastcmd:    "last cmd user sent to zork"
    }
  }
}
*/

exports.get = function() {

  return users;

}

exports.strList = function() { // get all current user nicks into a string and return
  var userStrList = "";
  for(var key in users) {
    var val = users[key];
    userStrList = userStrList +key+ ', ';
  }
  console.log(ink.itag+" USERS: ".info+userStrList); 
  return userStrList;
}


exports.sanitize_playername = function(nick) {
 nick = nick.replace(/[^a-zA-Z0-9\-\_]/g, "");
 nick = nick.replace("zork", "");
 nick = nick.replace("zork1", "");
 nick = nick.replace("zork2", "");
 nick = nick.replace("zork3", "");
 nick = nick.replace("zork-bot", "");
 nick = nick.replace("ZORK", "");
 nick = nick.replace("ZORK.DAT", "");
 nick = nick.replace("ZORK2.DAT", "");
 nick = nick.replace("ZORK3.DAT", "");
 nick = nick.replace("ZORK3.DAT", "");
 
 return nick;
}

exports.doSave = doSave;
function doSave(nick) {
 nick = nick.replace(/[^a-zA-Z0-9\-\_]/g, "");
 
 // Check if user directory exists and create if not.
	try {
	    // Query the entry
	    stats = fs.lstatSync('../saved/'+nick);
	    rmPlayerSavedGame(stats, nick); 
	} catch (e) { 
	    console.error(ink.etag+'%s', e);
	    console.log(ink.gtag+' Creating user directory ');
	    exec("mkdir ../saved/"+nick, puts);
	    callSave(nick);
	}		
}

function rmPlayerSavedGame(stats, nick) {
	        // Yes it is, so that means user file prob exists
	        // Remove it
	        exec("rm ../saved/"+nick+'/'+nick+"?", puts);
	        console.log(ink.itag+nick+' removed old game');
	        callSave(nick);	
}

function callSave(nick) {
					console.log(ink.itag+nick+' is requesting to save game');
					zork.writePlayerCmd(nick, 'save');//tell zork to write this command from the user
					console.log(ink.itag+'Performing save for: ' + nick);
					completeSave(nick);
}

function completeSave(nick) {
			 		zork.writePlayerCmd(nick, '../saved/'+nick+'/'+nick);//tell zork to write this command from the user
			 		eh.emit('sendPlayerMsg', nick, "Saved player game data. To load your game next time you play, type 'RELOAD'.");
}

exports.doLoad = doLoad;
function doLoad(nick) {
					nick = nick.replace(/[^a-zA-Z0-9\-\_]/g, "");
	
					console.log(ink.itag+nick+' is requesting to load his/her game');
					zork.writePlayerCmd(nick, 'restore');//tell zork to write this command from the user
					console.log(ink.itag+'Performing load for: ' + nick);
					completeLoad(nick);
}

function completeLoad(nick) {
			 		zork.writePlayerCmd(nick, '../saved/'+nick+'/'+nick);//tell zork to write this command from the user
			 		eh.emit('sendPlayerMsg', nick, "Loaded player game data. To save your game next time you quit playing, type 'SAVE'");
}



// Puts
function puts(error, stdout, stderr) { sys.puts(stdout) }