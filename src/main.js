#!/usr/bin/env node
//
// ZORK IRC BOT v0.0.4 - Codename Blamovision Edition
// Dedicated to the members of SubProto
// irc.subproto.com 7000 - SSL ONLY!
// 
// Developed by: Fatal and Fetter
// ShoutOuts to da'crew
// Fauxtonic, the3percent, pingray, moxi, chisleu, jaybles
// professorblak, frank and all the rest of you.
//  
// Shouts to THCNET.net /zork/ for
// being the inspiration of this project
// 
// And it wouldn't be possible without
// focom-if.org for hosting zork I, II, and III !
// 
// Respect,


// ---- INITAL LOAD OF APPLICATION

var fs = require('fs');

// CONFIGURATION
var config_data = JSON.parse(fs.readFileSync("./config.json", "utf8"));
exports.config_data = config_data;

var language = JSON.parse(fs.readFileSync("./language.json", "utf8"));
exports.language = language;

// Redfine the default console.log for our application
var redefine_console_log = require('./debuglayer').redefine_console_log;
redefine_console_log();

var syslog = require('./sysloglayer');

var colors = require('./colors').colors;
var banner = require('./colors').banner;
console.log(banner());

// Setup and export our event emitter so dependent modules can use it
var events = require('events');
console.log('Events imported  ...................  '.verbose+'[OK]'.info);

var mainEmitter = new events.EventEmitter();
exports.mainEmitter = mainEmitter;
console.log('Emitter activated  .................  '.verbose+'[OK]'.info);


// WEB LAYER INIT
if (config_data.configs.mods.web) {
  var websrvr = require('./webserver');
  console.log('Webserver Layer imported  ..........  '.verbose+'[OK]'.info);
  websrvr.start();
}


// MINECRAFT MODULE INIT
// Must edit config.js with minecraft bot creds
if (config_data.configs.mods.minecraft) {
	var mcl = require('./mclayer');
	console.log('Minecraft Layer imported  ..........  '.verbose+'[OK]'.info);
  mcl.start();// start the minecraft layer
}

// GAME LAYER INIT
// zork is our only game right now but later other games would go here too
var zork = require('./zorkiolayer');
console.log('Zork Layer imported  ...............  '.verbose+'[OK]'.info);

//mainEmitter.emit('playerstate', "");

// COMMUNICATIONS LAYER
if (config_data.configs.mods.irc) {
	var ircl = require('./irclayer.js');
	console.log('IRC Layer imported  ................  '.verbose+'[OK]'.info);
	// Start everything 


ircl.start();
}


// ZORK 2015 - SubProto - by Fatal and Fetter
