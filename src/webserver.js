// webserver.js - Web Server Layer

/* NOTES and TODO

   CONFIG - manage port numbers and any other business for this layer in config.json
*/

var ink   = require('./colors');
var mp    = require('./multiplayer');
var body = "";

var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var routing = require('./routing').routing;
var apireq = require('http');
//var request = require('request');

exports.io = io;

// setup event handler eh
var eh = require('./main').mainEmitter;

var write_apache_style_log = require('./sysloglayer').write_apache_style_log;

var config_data = require('./main').config_data;

var users       = require('./users');

function start() {

// Routing
routing(app);

// Start server
http.listen(config_data.configs.master.port, function(){
	console.log('listening on *:'+config_data.configs.master.port.toString());
});
 
}

exports.start = start;

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

/*  no needed unless socket.on is coming from a webpage, like for /zork page and /api SEE : routing.js
 socket.on('chat message', function(msg){
		io.emit('chat message', msg);
  });
 
  socket.on('stats message', function(msg){
		io.emit('stats message', msg);
	});
	
	socket.on('stream message', function(msg){
		io.emit('stream message', msg);
  });
 */
  
  // API Handler for Master
  socket.on('api message', function(msg){
  	if (config_data.configs.master.active == true){
			console.log('api request: ' + msg);
			 //io.emit('stream message', mp.doMpReport());
	  	 //io.emit('stats message', mp.doStatsReport());
		}
  });
  
});


exports.doStatsRequest = doStatsRequest;
function doStatsRequest(msg) {
	console.log('DOING REQUEST NOW');
		//The url we want is: 'www.random.org/integers/?num=1&min=1&max=10&col=1&base=10&format=plain&rnd=new'
	var options = {
	  host: 'http://192.168.1.109',
	  path: '/api?msg='+msg,
	  port: 8888
	};
	
	//var req = apireq.request(options, callback);
	//	req.end();
}

exports.doStreamRequest = doStreamRequest;
function doStreamRequest(msg) {
	console.log('DOING REQUEST NOW');

	var options = {
	  host: 'http://192.168.1.109',
	  path: '/api?msg='+msg,
	  port: 8888
	};

//	var req = apireq.request(options, callback);
//		req.end();
}


callback = function(response) {
	  var str = '';
	  //another chunk of data has been recieved, so append it to `str`
	  response.on('data', function (chunk) {
	    str += chunk;
	  });
	  //the whole response has been recieved, so we just print it out here
	  response.on('end', function () {
	    console.log(str);
	  });
	  
	}




