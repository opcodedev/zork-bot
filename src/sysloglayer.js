// syslog.js - System Logging Layer

// Exports
exports.write_apache_style_log = write_apache_style_log;

// FILE HANDLER
var fs = require('fs');

// CONFIGURATION
// load config data from main
var config_data = require('./main').config_data;

var debug = require('./debuglayer');
var colors = require('./colors').colors;

// RUN A TEST AT START
//write_apache_style_log("error", "nick", "mcplayer", "some_function_ran_by_player", "500", __line, __line, "details");


/*
 * Write our syslog events in an apache
 * style format compatible with logstalgia :)
 * 
 * SEE: https://code.google.com/p/logstalgia/wiki/CustomLogFormat
 * 
 * Required fields
 * timestamp - A unix timestamp of when the request occurred.
 * hostname - hostname of the request.
 * path - path being required.
 * response code - response code.
 * response size - The size of the response in bytes. 
 * 
 * Optional fields
 * success - 1 or 0 to indicate if the response was successful.
 * response colour - colour to use for response code to use in hex (#FFFFFF) format.
 * referrer url - the referrer url.
 * user agent - the user agent.
 * virtual host - the virtual host (to use with --paddle-mode vhost).
 * pid/other - process id or some other identifier (--paddle-mode pid). 
 * 
 * Examples
 * 
 * Using the required fields:
 * 
 * 1371769989|127.0.0.1|/index.html|200|1024
 * 
 * Including the optional fields:
 * 
 * 1371769989|127.0.0.1|/index.html|200|1024|1|00FF00|http://www.example.com/|Mozilla/5.0|webserver|1234
 * 
 * 
 * [Wed Oct 11 14:32:52 2000] [error] [client 127.0.0.1] client denied by server configuration: /export/home/
 * 174.107.156.74 - - [15/Dec/2014:09:03:34 -0500] "GET /dashboard/display?last_retrieved=1418652212690 HTTP/1.1" 200 1171 "http://162.2dashboard/index" "Mozilla/5.0 (X11; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0 Iceweasel/31.3.0"
 *
 * var string type - types: abuse, error
 * var string data
 * 
 * 
 * Example usuage: write_apache_style_log("error", "nick", "mcplayer", __function, "500", "1024, __line, "details");
 */


function write_apache_style_log(type, nick, mcplayer, func, respcode, data_size, line, details) {
	logging = config_data.configs["oper"].logging;
	
	if (logging) {
		
		mcplayer = mcplayer || "NA"
		
		var minimum_req = Math.floor(new Date() / 1000) + "|" + "ircnick-"+nick+"-mcp-"+mcplayer+".zork" + "|" + func + "|" + respcode + "|" + data_size + "|" + line + "|" + details;
		
		if (type == 'error')
	  	fs.appendFile('logs/error.log', minimum_req + "\r\n",function(err) {
	  	if(err) {
      	console.log(ink.itag+" SYSLOG LAYER:".error+__function.error+"():".error+__line.toString().error+ 
        " error: ".error +err.error);
       }
    });
       
	  if (type == 'abuse')
	  	fs.appendFile('logs/abuse.log', minimum_req + "\r\n",function(err) {
	  	if(err) {
      	console.log(ink.itag+" SYSLOG LAYER:".error+__function.error+"():".error+__line.toString().error+ 
        " error: ".error +err.error);
       }
    });
    
    if (type == 'access')
	  	fs.appendFile('logs/access.log', minimum_req + "\r\n",function(err) {
	  	if(err) {
      	console.log(ink.itag+" SYSLOG LAYER:".error+__function.error+"():".error+__line.toString().error+ 
        " error: ".error +err.error);
       }
    });
	  
 }
}