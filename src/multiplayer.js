// custom multiplayer layer

var ink         = require('./colors');
var eh          = require('./main').mainEmitter;
var users       = require('./users');
var zork        = require('./zorkiolayer');

// catch command and copy to zork.lastcmd
exports.catchCmd = function (users, nick, msg) {

  res = msg.toLowerCase();
  msg = res;
  // The following check should be unneeded since this is initialized at game start now.
    if (users[nick].zork === undefined) { 
      users[nick].zork = {};
      users[nick].zork.cmdcnt = 0;
    }

    if (msg == "diagnose") {
      users[nick].zork.lastsyscmd = msg;
      users[nick].zork.issyscmd = true;
    }
    else {
      if (users[nick].zork.lastcmd == msg) users[nick].zork.cmdcnt++;
      else users[nick].zork.cmdcnt = 1;

      users[nick].zork.lastcmd = msg;
      users[nick].zork.issyscmd = false;
    }
    console.log(ink.wtag+" MP LAYER: ".warn+nick+": caught '".warn+msg+"'".warn);
  //}
}


// grab the direct zork health response 
//- a smart one will interpret these dumb responses and cook up a new health report
exports.parseMultiplayerCmds = function (nick, data) {

  var usrs = users.get();
  if ((usrs[nick] !== undefined) && (usrs[nick].zork !== undefined) && (usrs[nick].zork.lastcmd !== undefined)) {
    if ((usrs[nick].zork.lastsyscmd == 'diagnose') && (usrs[nick].zork.issyscmd)) { 
      // manage players health for MP
      usrs[nick].zork.health = data;
      console.log(ink.itag+' MP LAYER: '.info+nick+"'s"+" health: ".error);
    }
    else { // for any command we arent handling, save response to zork.lastscene
      usrs[nick].zork.lastlastscene = usrs[nick].zork.lastscene;
      usrs[nick].zork.lastscene = data;
      console.log(ink.itag+' MP LAYER: set '.info+nick+'.zork.lastscene '.info);
    }
  }
}


// trigger the catchDiagnose sequence by sending the diagnose command to zork
exports.getHealthDumb = function(nick) {
  //eh.emit('zork_writePlayerCmd', nick, 'diagnose');
  zork.writePlayerCmd(nick, 'diagnose');
  console.log(ink.itag+' MP LAYER: sending diagnose sequence to get player health'.info);
}


// compare all user locations to each other
// and add any users at same location to this players scene
exports.doLocations = function (nick) {

  var sceneUserStr = "", usrs = users.get();
  var usrsInSceneObj;
  for(var key in usrs) { 
    if ((usrs[key] != usrs[nick]) &&
       (usrs[key].zork.lastscene == usrs[nick].zork.lastscene)) {
      if (sceneUserStr == "") {
        sceneUserStr += key;
        if (usrsInSceneObj === undefined) usrsInSceneObj = {};
        usrsInSceneObj[key] = key;
        if(usrs[nick].zork.usersInScene[key] === undefined) {
          usrs[nick].zork.usersInScene[key] = key;
          eh.emit('sendPlayerMsg', key, nick+" is here.");
        }
        if(usrs[key].zork.usersInScene[nick] === undefined) {
          usrs[key].zork.usersInScene[nick] = nick;
          eh.emit('sendPlayerMsg', nick, key+" is here.");
        }
      } else {
        sceneUserStr += ", "+key;
        usrsInSceneObj[key] = key;
        if(usrs[nick].zork.usersInScene[key] === undefined) {
          usrs[nick].zork.usersInScene[key] = key;
          eh.emit('sendPlayerMsg', key, nick+" is here.");
        }
        if(usrs[key].zork.usersInScene[nick] === undefined) {
          usrs[key].zork.usersInScene[nick] = nick;
          eh.emit('sendPlayerMsg', nick, key+" is here.");
        }
      }
    }
    else if ((usrs[key].zork.lastscene != usrs[nick].zork.lastscene)) { 

      if (usrs[nick].zork.usersInScene[key] !== undefined) {
        delete usrs[nick].zork.usersInScene[key];
        eh.emit('sendPlayerMsg', key, nick+" did: "+usrs[nick].zork.lastcmd);
      }
      if (usrs[key].zork.usersInScene[nick] !== undefined) {
        delete usrs[key].zork.usersInScene[nick];
        //eh.emit('sendPlayerMsg', key, nick+" did: "+usrs[nick].zork.lastcmd);
      }
    }
  }
  return "";
}


// create a string report of all users and stats
exports.doMpReport = function () {
  var reportStr = "";
  var usrs = users.get();  

  for(var key in usrs) {
    reportStr += "<br /><br />"+ key+":<br />";
    if(usrs[key].zork !== undefined) {
      if(usrs[key].zork.health !== undefined)
        reportStr += "Health: <br />"+usrs[key].zork.health+"<br />";
      if(usrs[key].zork.lastcmd !== undefined)
        reportStr += "Last Player Command: <br />"+usrs[key].zork.lastcmd+"<br />";
      if(usrs[key].zork.lastsyscmd !== undefined)
        reportStr += "Last System Command: <br />"+usrs[key].zork.lastsyscmd+"<br />";
      if(usrs[key].zork.lastscene !== undefined) {
        reportStr += "Last Scene: <br />"+usrs[key].zork.lastscene+"<br />"; 
      }
    }
  }
  return reportStr;
}

exports.doStatsReport = function () {
	var reportStr = "";
  var usrs = users.get();
  for(var key in usrs) {
  }
  
  reportStr += users.strList();
  
	return reportStr;

}


exports.doHealthReport = function (nick) {

  var healthStr = "Health: Implementing";

  return healthStr;
}
