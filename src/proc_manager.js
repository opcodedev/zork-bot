
var dbg         = require('./debuglayer');
var fs          = require('fs');
var ink         = require('./colors');
var spawn       = require('child_process').spawn;
var users       = require('./users');


exports.spawnsub = spawnsub;

function spawnsub(nick, cmd, args){ // will need to extend this to variadic args later
  var child = 
  spawn(cmd, args, { stdio: ['pipe', 'pipe', 'pipe'] } 
  //cp.exec('ls ' //+version_dat_file  
    ,function (error, stdout, stderr) {
      //console.log("start: " +error+stderr);
      console.log(ink.etag+' PROC LAYER:'.error+__function.error+"():".error+__line.toString().error+
        ' stdout: '.error );
      console.log(ink.etag+' PROC LAYER:'.error+__function.error+"():".error+__line.toString().error+
        ' stderr: '.error + stderr.error);
      write_apache_style_log("error", nick, "", __function, "500", __line, __line, "Game execution error");
      if (error !== null) {
        console.log(ink.etag+' PROC LAYER:'.error+__function.error+"():".error+__line.toString().error+
          ' Execution error: '.error + error.error);
        write_apache_style_log("error", nick, "", __function, "500", __line, __line, "Game execution error");
      }   
  }); 
  var subprocs;
  subprocs = users.get();

  // need to do proper error checking
  console.log(ink.itag+' PROC LAYER: '.info+nick+' spawned '.info+cmd+' '+args);

  // add subproc entry for this subproc. nick is the key
  // need to make sure we dont create duplicates in here 
  // and setup defaults
  subprocs[nick] = {};
  subprocs[nick].nick = nick;
  subprocs[nick].child = child; 
  subprocs[nick].debug = false;  

  for(var key in subprocs[nick]) {
    var val = subprocs[nick][key];
    console.log(ink.itag+" PROC LAYER: head: ".info+nick+ " Key: "+key+" value:"+val);
  }
  return subprocs;
}
