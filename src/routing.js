
exports.routing = function (app) {
	// Routing
	app.get('/', function(req, res){
		res.sendFile(__dirname + '/html/index.html');
	});
	
	app.get('/zork', function(req, res){
		res.sendFile(__dirname + '/html/zork.html');
	});
	
	app.get('/stream', function(req, res){
		res.sendFile(__dirname + '/html/stream.html');
	});
	
	app.get('/stats', function(req, res){
		res.sendFile(__dirname + '/html/stats.html');
	});
	
	app.get('/api', function(req, res){
		res.sendFile(__dirname + '/html/api.html');
	});
	
	
	//images
	app.get('/imgs/zork-1-map.jpg', function(req, res){
		res.sendFile(__dirname + '/html/imgs/zork-1-map.jpg');
	});
	
	app.get('/imgs/stone.jpg', function(req, res){
		res.sendFile(__dirname + '/html/imgs/stone.jpg');
	});
	
	app.get('/imgs/topmenu.png', function(req, res){
		res.sendFile(__dirname + '/html/imgs/topmenu.png');
	});
	
	app.get('/imgs/bg.png', function(req, res){
		res.sendFile(__dirname + '/html/imgs/bg.png');
	});
	
	app.get('/imgs/bgtop.jpg', function(req, res){
		res.sendFile(__dirname + '/html/imgs/bgtop.jpg');
	});
	
	app.get('/imgs/health.png', function(req, res){
		res.sendFile(__dirname + '/html/imgs/health.png');
	});
}