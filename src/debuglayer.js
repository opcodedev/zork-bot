// debuglayer.js - Debugging Layer


// Exports
exports.redefine_console_log = redefine_console_log;

// CONFIGURATION
var config_data = require('./main').config_data;

// CONSOLE.LOG REDEFINE
function redefine_console_log() {
	
	debug = config_data.configs["oper"].debug;

	if(!debug) {
		console.log = function() {};
	}
	
	if (debug) {
		console.log = function(data) { require('util').log(data); };
	}
	
}

// Object Defined Properties

Object.defineProperty(global, '__stack', {
get: function() {
        var orig = Error.prepareStackTrace;
        Error.prepareStackTrace = function(_, stack) {
            return stack;
        };
        var err = new Error;
        Error.captureStackTrace(err, arguments.callee);
        var stack = err.stack;
        Error.prepareStackTrace = orig;
        return stack;
    }
});

Object.defineProperty(global, '__line', {
get: function() {
        return __stack[1].getLineNumber();
    }
});

Object.defineProperty(global, '__function', {
get: function() {
        return __stack[1].getFunctionName();
    }
});


/* example use

function foo() {
    console.log(__line);
    console.log(__function);
}
*/
