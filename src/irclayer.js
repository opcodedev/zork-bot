// irclayer.js - IRC Layer

var irc         = require('irc');
var ink         = require('./colors');
var badchars    = require('./filters.js').badchars;
var dbg         = require('./debuglayer');

var about       = require('./irc_commands/about');
var help        = require('./irc_commands/help');
var startzork   = require('./irc_commands/startzork');
var endzork     = require('./irc_commands/endzork');
var mp    = require('./multiplayer');

var users       = require('./users');
var zork        = require('./zorkiolayer');

var io = require('./webserver').io;
var doStreamRequest = require('./webserver').doStreamRequest;
var doStatsRequest = require('./webserver').doStatsRequest;
var write_apache_style_log = require('./sysloglayer').write_apache_style_log;

// EVENT HANDLING
// setup event handler eh
var eh = require('./main').mainEmitter;

// CONFIGURATION
// load config data from main
var config_data = require('./main').config_data;

  
var bot = bot;


eh.on('sendPlayerMsg', function(nick, msg) {
 	bot.say(nick, msg);
  //zork.writePlayerCmd(nick, 'diagnose');
});


exports.start = function() {

  bot   = new irc.Client(config_data.configs.irc.server, config_data.configs.irc.botname, {
    debug: config_data.configs.irc.debug,
    showErrors: config_data.configs.irc.showErrors,
    userName: config_data.configs.irc.username,
    realName: config_data.configs.irc.realname,
    port: config_data.configs.irc.port,
    secure: config_data.configs.irc.secure,
    selfSigned: config_data.configs.irc.selfSigned,
    certExpired: config_data.configs.irc.certExpired,
    channels: [config_data.configs.irc.channel]
  });

  bot.addListener('pm', ircmain);

  // Bot error listener
  bot.addListener('error', function(message) {
    //console.error('ERROR: %s: %s',  message.args.join(' '));
    console.error(ink.etag+' IRC LAYER: ERROR: %s: %s'.error, message.command, message.args.join(' '));
  });

  console.log('IRC Layer online  ..................  '.verbose+'[OK]'.info);
}


//Bot Private Message listener
function ircmain(nick, message) {

  // sanitize user input
  message = message.replace(/[^a-zA-Z0-9\s]/g, "").toLowerCase();;
	nick = users.sanitize_playername(nick);
 
  var usrs = users.get();
  
  if (message.substring(0,3) == 'sz ' || message.substring(0,3) == ' sz '&& 
    Number(message.substring(3)) && message.substring(3) <= 3 ) {

    // Start Zork I, II, III
    startzork.run(nick, message);
    
  }  else if (message == 'help') { help.runhelp(nick, message);// Help Instructions

  } else if (message.substring(0,5) == 'help ') { help.runhelp(nick, message);// Help Pages

  } else if (message == 'about') { about.run(nick);// About Zork Bot

  }  else {
  	// If player is in our players list then 
    if (usrs[nick] !== undefined) {
    	
    		if (message == 'ez') { endzork.run(nick); // End Zork game
			    
			  } else if (message.substring(0,3) == 'pl ') {  // list all players and their lastscene (pl - player location)

			      var n = message.substring(3);
			      if(usrs[n] !== undefined)
			        eh.emit('sendPlayerMsg', nick, n+" is at: "+usrs[n].zork.lastscene);
			
			  } else if (message.substring(0,3) == 'pc ') { // party chat (pc) with everyone in party (whole game right now)
			  	
				      for(var key in usrs) { 
				         if(key != nick) 
				            eh.emit('sendPlayerMsg', key, nick+" says: "+message.substring(3));
				      }
				      
			  }  else if (message.substring(0,5) == 'save') { users.doSave(nick); // save players game
			       
			  } else if (message.substring(0,7) == 'reload') { users.doLoad(nick); // load players game
			       
			  } else {
    	
	        console.log(ink.wtag+" IRC LAYER: got message from ".warn+nick.verbose+":".prompt+message.verbose);
	        io.emit('stream message', mp.doMpReport());
	        if (config_data.configs.slave.active == true){ doStreamRequest(mp.doMpReport().replace('<br />','')); }
	        io.emit('stats message', mp.doStatsReport());
	        if (config_data.configs.slave.active == true){ doStatsRequest(mp.doStatsReport().replace('<br />','')) }
	        zork.writePlayerCmd(nick, message);//tell zork to write this command from the user
	        //zork.writePlayerCmd(nick, 'diagnose'); // send this along every time to keep health updated
       }
    }
    else { 
      // If active game not started for player show quick_help about how they can get started
      if (message != "help" && message.substring(0,5) != 'help ' && !Number(message.substring(5))) {
        help.runquickhelp(nick, message);
      }
    }
  }
}


exports.getBot = function() {
  return bot;
}

