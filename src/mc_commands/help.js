var ink         = require('../colors');
var dbg         = require('../debuglayer');
var help        = require('../helpers.js').help;
var quick_help  = require('../helpers.js').quick_help;

var write_apache_style_log = require('../sysloglayer').write_apache_style_log;

var mcl       = require('../mclayer');

exports.runhelp = function(nick, message) {


  if (Number(message.substring(5))) {
    try {
    	mcl.sendZorkMCMsg(nick.replace("mc-",""), "/msg ", 70, help(message.substring(5)));
      //bot.say(nick, help(message.substring(5)));
    } catch (ex)  {
      console.log("Error displaying help information".error );
     // write_apache_style_log("error", nick, mcplayer, __function, "500", __line, __line, "Error displaying help information");
    }
  } else {
    try {
    	mcl.sendNoticeMCMsg(nick.replace("mc-",""), "See More Help:\n\n"+"help <1-5>\n");
    } catch (ex)  {
      console.log("Error displaying help information".error );
     // write_apache_style_log("error", nick, mcplayer, __function, "500", __line, __line, "Error displaying help information");
    }
  }
}

exports.runquickhelp = function(nick, message) {
  if (message != "help" && message.substring(0,5) != 'help ' && !Number(message.substring(5))) {
   mcl.sendZorkMCMsg(nick.replace("mc-",""), "/msg ", 70, quick_help(nick));
  }
}
