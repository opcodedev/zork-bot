var ink         = require('../colors');
var dbg         = require('../debuglayer');
var help        = require('../helpers.js').help;
var about        = require('../helpers.js').about;
var write_apache_style_log = require('../sysloglayer').write_apache_style_log;

var mcl        = require('../mclayer');


exports.run = function (nick) {

  try {
      mcl.sendZorkMCMsg(nick.replace("mc-",""), "/msg ", 70, about());
  } catch (ex)  {   
    console.log("Error displaying about information " + ex.error );  
   // write_apache_style_log("error", nick, mcplayer, __function, "500", __line, __line, "Error displaying help information");
  }
}
