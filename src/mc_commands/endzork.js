var irc         = require('irc');
var ink         = require('../colors');
var dbg         = require('../debuglayer');
var config_data = require('../main').config_data;
var ver         = require('../version.js');
var users       = require('../users');
var zork        = require('../zorkiolayer');
var write_apache_style_log = require('../sysloglayer').write_apache_style_log;

var mcl       = require('../mclayer');

// EVENT HANDLING
// setup event handler eh
var eh = require('../main').mainEmitter;

var version, version_path, version_dat_file;

var write_apache_style_log = require('../sysloglayer').write_apache_style_log;


exports.run = function(nick) {

  mcl.sendNoticeMCMsg(nick.replace("mc-",""), 'Killing frotz for: ' +nick);

  zork.endzork(nick);

  mcl.sendNoticeMCMsg(nick.replace("mc-",""), 'Ended the game!');

}
