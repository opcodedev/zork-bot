var colors         = require('../colors').colors;
var dbg         = require('../debuglayer');
var players     = require('../main').players;
var config_data = require('../main').config_data;
var ver         = require('../version.js');
var write_apache_style_log = require('../sysloglayer').write_apache_style_log;
var language = require('../main').language;

var zork        = require('../zorkiolayer');

var mcl       = require('../mclayer');

// EVENT HANDLING
// setup event handler eh
var eh = require('../main').mainEmitter;

var version, version_path, version_dat_file;

// EVENT HANDLING
// setup event handler eh
var eh = require('../main').mainEmitter;

var mcl       = require('../mclayer');

var users       = require('../users');

exports.run = function(nick, zork_cmd) {
	
	var usrs = users.get();

  if (usrs[nick] !== undefined) {
  	mcl.sendNoticeMCMsg(nick.replace("mc-",""),  language.lang["default"].playerfound);
	} else {// player is not found so add player and continue

    mcl.sendNoticeMCMsg(nick.replace("mc-",""),  language.lang["default"].playernotfound);
    
		version = parseInt(zork_cmd.substring(2).replace(" ",""));

    if (Number(version)) {
      version_path = ver.get_version_path(version);
      version_dat_file = ver.get_version_dat_file(version);
    }

    // send a signal to zork to spawn the game
    console.log('MINECRAFT LAYER: sending signal to zork to spawn game'.warn);
    zork.startzork(nick, version, version_path+'/'+version_dat_file);
		
		mcl.sendNoticeMCMsg(nick.replace("mc-",""), language.lang["default"].spawn);

		mcl.sendNoticeMCMsg(nick.replace("mc-",""), language.lang["default"].playerlist + users.strList());
 }

}
