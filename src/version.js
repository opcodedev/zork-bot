
var config_data = require('./main').config_data;

var version = "";
var version_path = "";
var version_dat_file = "";

exports.get_version_path = get_version_path;
exports.get_version_dat_file = get_version_dat_file;

function get_version_path(version) {

  if (version == 1) {
    version_path = config_data.configs["paths"].zork1_dat_path;
  } else if (version == 2) {
    version_path = config_data.configs["paths"].zork2_dat_path;
  } else if (version == 3) {
    version_path = config_data.configs["paths"].zork3_dat_path;
  } else {
    version_path = config_data.configs["paths"].zork1_dat_path;
  }

  return version_path;

}

function get_version_dat_file(version) {

  if (version == 1) {
    version_dat_file = config_data.configs["paths"].zork1_dat_file;
  } else if (version == 2) {
    version_dat_file = config_data.configs["paths"].zork2_dat_file;
  } else if (version == 3) {
    version_dat_file = config_data.configs["paths"].zork3_dat_file;
  } else {
    version_dat_file = config_data.configs["paths"].zork1_dat_file;
  }

  return version_dat_file;

}

