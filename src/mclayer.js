/*
 * Zork Arcade Minecraft Bot Client
 * 
 * This bot is used for handling communications
 * between zork and minecraft players.
 * 
 */

// CONFIGURATION
// load config data from main
var config_data = require('./main').config_data;

var zork        = require('./zorkiolayer');

// EVENT HANDLING
// setup event handler eh
var eh = require('./main').mainEmitter;

var io = require('./webserver').io;

var users       = require('./users');

// Zork Vars
var zork_cmd 		= "";
var ink         = require('./colors');
var zork        = require('./zorkiolayer');
var startzork   = require('./mc_commands/startzork');
var endzork   	= require('./mc_commands/endzork');
var help        = require('./mc_commands/help');
var about        = require('./mc_commands/about');

var players     = require('./main').players;

// Minecraft Client Vars
var readline = require('readline');
var color = require("ansi-color").set;
var mc = require('minecraft-protocol');
var states = mc.protocol.states;
var util = require('util');

var dbgcolors = require('colors');

var colors = new Array();
colors["black"] = 'black+white_bg';
colors["dark_blue"] = 'blue';
colors["dark_green"] = 'green';
colors["dark_aqua"] = 'cyan'
colors["dark_red"] = 'red'
colors["dark_purple"] = 'magenta'
colors["gold"] = 'yellow'
colors["gray"] = 'black+white_bg'
colors["dark_gray"] = 'black+white_bg'
colors["blue"] = 'blue'
colors["green"] = 'green'
colors["aqua"] = 'cyan'
colors["red"] = 'red'
colors["light_purple"] = 'magenta'
colors["yellow"] = 'yellow'
colors["white"] = 'white'
colors["obfuscated"] = 'blink'
colors["bold"] = 'bold'
colors["strikethrough"] = ''
colors["underlined"] = 'underlined'
colors["italic"] = ''
colors["reset"] = 'white+black_bg'


var dictionary = {};
dictionary["chat.stream.emote"] = "(%s) * %s %s";
dictionary["chat.stream.text"] = "(%s) <%s> %s";
dictionary["chat.type.achievement"] = "%s has just earned the achievement %s";
dictionary["chat.type.admin"] = "[%s: %s]";
dictionary["chat.type.announcement"] = "[%s] %s";
dictionary["chat.type.emote"] = "* %s %s";
dictionary["chat.type.text"] = "<%s> %s";

var msg = undefined;

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});


if (!config_data.configs["minecraft"].host || !config_data.configs["minecraft"].port || !config_data.configs["minecraft"].user || !config_data.configs["minecraft"].passwd) {
    console.log("Please edit config.js : Section 'minecraft'");
    process.exit(1);
}


// Minecraft Client Settings
var host = config_data.configs["minecraft"].host;
var port = config_data.configs["minecraft"].port;
var user = config_data.configs["minecraft"].user;
var passwd = config_data.configs["minecraft"].passwd;

 
if (host.indexOf(':') != -1) {
    port = host.substring(host.indexOf(':')+1);
    host = host.substring(0, host.indexOf(':'));
}


var client = client;

// Handle Client Chat Messages 
var chats = [];

var privmsg = "/msg ";
var length = 70;

exports.start = function() {
	// Create Client Connection 
	 client = mc.createClient({
	    host: host,
	    port: port,
	    username: user,
	    password: passwd
	});

	// Client STATES change 
	client.on([states.PLAY, 0x40], function(packet) {
	    console.info('Kicked for ' + packet.reason);
	    process.exit(1);
	});
	 
	client.on('connect', function() {
	    console.log('Minecraft Layer online  ............  '.verbose+'[OK]'.info);
	});
	
	client.on('state', function(newState) {
	  if (newState === states.PLAY) {
	    chats.forEach(function(chat) {
	      client.write(0x01, {message: chat});
	    });
	  }
	});
	 
	rl.on('line', function(line) {
	    if(line == '') {
	        return;
	    } else if(line == '/quit') {
	        //var reason = 'disconnect.quitting';
	        //console.info('Disconnected from ' + host + ':' + port);
	        //client.write([states.PLAY, 0x40], { reason: reason });
	        console.info('Forcibly ended client');
	        process.exit(0);	
	        return;
	    }
	    if (!client.write([states.PLAY, 0x01], { message: line })) {
	      chats.push(line);
	    }
	
	});  
	 
	client.on([states.PLAY, 0x02], function(packet) {
		var zork_cmd = "";
	    var j = JSON.parse(packet.message);
	    var chat = parseChat(j, {});
	    //console.info(chat);
	    
	    // Split incoming data
	    var arr 		= chat.split(" ");
	  	var nick 		= arr[0];
	  	var msgtype 	= arr[1];
	  	
	    // Got a private msg from MC user.
	    if (msgtype == "whispers") {
	    		//console.info(chat + " " + nick + " " + msgtype);
	    		zork_cmd = chat.replace(nick + " ","");
					zork_cmd = zork_cmd.replace(msgtype + " ","");
					//console.log("Zork Cmd: " + zork_cmd);
	    }
    
      // sanitize user input
			zork_cmd = zork_cmd.replace(/[^a-zA-Z0-9\s]/g, "");
			nick = users.sanitize_playername(nick);
			
			// tag mc user
			nick = "mc-" + nick;
 
			var usrs = users.get();
	    
	    
      // Start Zork I, II, III
		  if (zork_cmd.substring(0,3) == 'sz ' || zork_cmd.substring(0,3) == ' sz '&& 
		    Number(zork_cmd.substring(3)) && zork_cmd.substring(3) <= 3 ) { 
		    startzork.run(nick, zork_cmd);
		    
		  } else if (zork_cmd == 'ez') { 
		  	endzork.run(nick);// End Zork game
		  	
		  } else if (zork_cmd == 'help') { help.runhelp(nick, zork_cmd);// Help Instructions

  		} else if (zork_cmd.substring(0,5) == 'help ') { help.runhelp(nick, zork_cmd);// Help Pages
  	
  		} else if (zork_cmd == 'about') { about.run(nick);// About Zork Bot
  			 
  		}else {// If we made it here then command MAY BE meant for the zork
  		// If player is in our users then 
  	
			    if (usrs[nick] !== undefined) {
			        //console.log(ink.wtag+" MC LAYER: got message from "+nick+":"+msg.verbose);
							io.emit('chat message', { for: 'everyone' });
			        //eh.emit('zork_writePlayerCmd', nick, zork_cmd);//tell zork to write this command from the user
			        zork.writePlayerCmd(nick, zork_cmd);//tell zork to write this command from the user
			    }
			    else { //nosuch player 
			      // If active game not started for player show quick_help
			     	if (zork_cmd != "" && zork_cmd != "help" && zork_cmd.substring(0,5) != 'help ' && !Number(zork_cmd.substring(5))) {
			        help.runquickhelp(nick, zork_cmd);
			      }
	    		}
	    }
	});
	
	eh.on('sendPlayerMCMsg', function(nick, msg) {
		// cut the reply for zorkio into 70 char chunks
		
		var nick = nick.replace("mc-", "")
		
		sendZorkMCMsg(nick, privmsg, length, msg);
		
		// Examples of using server / plugin commands on MC Server
		// This could be mapped to an json object and checked for when needed
		
		// detect start of game
		// and send player to zork area
		if(msg.indexOf("mailbox") > -1) {
			console.log(ink.wtag+' MINECRAFT LAYER: teleporting '.info +nick.verbose + " to Zork starting area.".info);
			runMCServerCmd("/tp", nick, "476.3786 65.50 1795.1113077");
		}
		
		// South side of starting house
		if(msg.indexOf("south side of a white house") > -1) {
			console.log(ink.wtag+' MINECRAFT LAYER: teleporting '.info +nick.verbose + " to south side of the white house.".info);
			runMCServerCmd("/tp", nick, "483.22913 65.0 1824.246");
		}
		
		// Back of starting house
		if(msg.indexOf("behind the white house") > -1) {
			console.log(ink.wtag+' MINECRAFT LAYER: teleporting '.info +nick.verbose + " to behind the white house.".info);
			runMCServerCmd("/tp", nick, "502.85996 64.000 1805.88280");
		}
		
	});
	
msg = undefined;
}


// example: sends a public chat message
//client.write(0x01, {message: "/msg " + nick +" test"});
//chats.push("/msg " + nick +" test");

exports.sendZorkMCMsg = sendZorkMCMsg;
function sendZorkMCMsg(nick, privmsg, length, msg) {
	var b = [];
    for(var i = length; i < msg.length; i += length){

       client.write(0x01, {message: privmsg + nick + " " + msg.slice(i-length, i)});
			 chats.push(privmsg + nick + " " +  msg.slice(i-length, i));
    }
    client.write(0x01, {message: privmsg + nick + " " + msg.slice(msg.length - (length - msg.length % length))});
		chats.push(privmsg + nick + " " +  msg.slice(msg.length - (length - msg.length % length)));
}

exports.sendNoticeMCMsg = sendNoticeMCMsg;
function sendNoticeMCMsg(nick, msg) {
		client.write(0x01, {message: "/msg " + nick + " " + msg});
		chats.push("/msg " + nick + " " + msg);
}

exports.runMCServerCmd = runMCServerCmd;
function runMCServerCmd(cmd, nick, data) {
		// send a command to minecraft
		client.write(0x01, {message: cmd + " " + nick + " " + data});
		chats.push(cmd + " " + nick + " " + data);
}

function parseChat(chatObj, parentState) {
  function getColorize(parentState) {
    var myColor = "";
    if ('color' in parentState) myColor += colors[parentState.color] + "+";
    //if (parentState.bold) myColor += "bold+";
    //if (parentState.underlined) myColor += "underline+";
    //if (parentState.obfuscated) myColor += "obfuscated+";
    //if (myColor.length > 0) myColor = myColor.slice(0,-1);
    return myColor;
  }
  if (typeof chatObj === "string") {
    return color(chatObj, getColorize(parentState));
  } else {
    var chat = "";
    //if ('color' in chatObj) parentState.color = chatObj['color'];
    //if ('bold' in chatObj) parentState.bold = chatObj['bold'];
    //if ('italic' in chatObj) parentState.italic = chatObj['italic'];
    //if ('underlined' in chatObj) parentState.underlined = chatObj['underlined'];
    //if ('strikethrough' in chatObj) parentState.strikethrough = chatObj['strikethrough'];
    //if ('obfuscated' in chatObj) parentState.obfuscated = chatObj['obfuscated'];

    if ('text' in chatObj) {
      chat += color(chatObj.text, getColorize(parentState));
    } else if ('translate' in chatObj && dictionary.hasOwnProperty(chatObj.translate)) {
      var args = [dictionary[chatObj.translate]];
      chatObj['with'].forEach(function(s) {
        args.push(parseChat(s, parentState));
      });

      chat += color(util.format.apply(this, args), getColorize(parentState));
    }
    for (var i in chatObj.extra) {
      chat += parseChat(chatObj.extra[i], parentState);
    }
    return chat;
  }
}

exports.getMCClient = function() {
  return client;
}
