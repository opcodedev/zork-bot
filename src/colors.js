// colors.js - Console Logging Colors Layer


var colors = require('colors');

// Exports
exports.colors = colors;
exports.banner = banner;




// Color Set Theme
colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});

exports.itag = '['.input.bold+'+'.info.bold +']'.input.bold;
exports.etag = '['.input.bold+'-'.error.bold+']'.input.bold;
exports.wtag = '['.input.bold+'!'.warn.bold +']'.input.bold;
exports.dtag = '['.help.bold +'*'.warn.bold +']'.help.bold;




// Banner - Will randomize these
function banner () {
	
	bannerInt = randomInt(0,3);
	
	switch (bannerInt) {
    case 0:
        	banner = "\n\n";
					banner += "************************************************\n".error;   
					banner += "*                                              *\n".error;                           
					banner += "*      ".error+"_/_/_/_/_/                      _/".warn+"      *\n".error;
					banner += "*           ".error+"_/      _/_/    _/  _/_/  _/  _/".warn+"   *\n".error;
					banner += "*        ".error+"_/      _/    _/  _/_/      _/_/".warn+"      *\n".error;
					banner += "*     ".error+"_/        _/    _/  _/        _/  _/".warn+"     *\n".error;
					banner += "*  ".error+"_/_/_/_/_/    _/_/    _/        _/    _/".warn+"    *\n".error;
					banner += "*                                              *\n".error;
				  banner += "************************************************\n".error;    
				  banner += "*   ".error+"Developed by Fatal and Fetter - SubProto".warn+"   *\n".error;
				  banner += "************************************************\n\n".error;
        break;
    case 1:
        	banner = "\n\n";
					banner += "************************************************\n".silly;   
					banner += "*                                              *\n".silly;                           
					banner += "*      ".silly+"_/_/_/_/_/                      _/".silly+"      *\n".silly;
					banner += "*           ".silly+"_/      _/_/    _/  _/_/  _/  _/".silly+"   *\n".silly;
					banner += "*        ".silly+"_/      _/    _/  _/_/      _/_/".silly+"      *\n".silly;
					banner += "*     ".silly+"_/        _/    _/  _/        _/  _/".silly+"     *\n".silly;
					banner += "*  ".silly+"_/_/_/_/_/    _/_/    _/        _/    _/".silly+"    *\n".silly;
					banner += "*                                              *\n".silly;
				  banner += "************************************************\n".silly;    
				  banner += "*   ".silly+"Developed by Fatal and Fetter - SubProto".silly+"   *\n".silly;
				  banner += "************************************************\n\n".silly;
        break;
        
    case 2:  
    			banner = "\n\n";      
				  banner += "888 88b, 888                                         ,e,       ,e,                   \n".silly;
				  banner += "888 88P' 888  ,'Y88b 888 888 8e   e88 88e  Y8b Y888P  '   dP'Y ''   e88 88e  888 8e  \n".silly;
				  banner += "888 8K   888 '8' 888 888 888 88b d888 888b  Y8b Y8P  888 C88b  888 d888 888b 888 88b \n".silly;
				  banner += "888 88b, 888 ,ee 888 888 888 888 Y888 888P   Y8b '   888  Y88D 888 Y888 888P 888 888 \n".silly;
				  banner += "888 88P' 888 '88 888 888 888 888  '88 88'     Y8P    888 d,dP  888  '88 88'  888 888 \n".silly;
				break;
				
				
	} 
  
  return banner;                                      
}                                      

// Generate a random int for selecting a banner
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
