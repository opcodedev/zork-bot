// Exports
exports.badchars = badchars;


exports.cutFirstLine = function(data) {

  data = data.replace(/.*\n/,"");

  return data;
}


exports.removeNewlines = function(data) {

  data = data.replace(/[\r\n]/g, " ");

  return data;
}


function badchars(data) {
   	data = data.replace(/[\x00\x08\x0F]/g, "");
   	data = data.replace(/\x1B[^\x1B]/g, "_");

    //data = data.replace(/_.+_/g, "_");
    //while (/_+/g.test(data)) {
   	  //data = data.replace(/_+/g, "_");
   	  //data = data.replace(/_./g, "_");
    //}

    //data = data.replace(/_\?[0-9]+[a-zA-Z]/g, "");

		data = data.replace(/[^a-zA-Z0-9\x0d\;\.\,\!\?\_\#\'\"\/\\\-\:\(\)\s\n]/g, "");
		data = data.replace(/[\x0d\x0a]/g, " ");
    /*
		data = data.replace(/\[K/g, "");
		data = data.replace(/\[2B/g, "");
		data = data.replace(/\[1B/g, "");
		data = data.replace(/\[18B/g, "");
		data = data.replace(/\[[0-9];[0-9]/g, "");
		data = data.replace(/\[[0-9];[0-9][0-9]/g, "");
		data = data.replace(/\[[0-9][0-9]/g, "");
		data = data.replace(/\[[0-9]MM/g, "");
		data = data.replace(/\[m/g, "");
    */
/*
		//data = data.replace(/[^\[\w\s\d\n\t]/g, "");
		data = data.replace(/[\x0d\x0a]/g, "\n");
		//data = data.replace(/\nm/g, "\n");
		//data = data.replace(/[\f\v\0]/g, "");
		data = data.replace(/\[[12][B]/g, "");
		data = data.replace(/\[\?[0-9]*[a-zA-Z]/g, "");
		data = data.replace(/\[[0-9]*[BA]/g, "");
		data = data.replace(/\[[0-9]*MM/g, "");
		data = data.replace(/\[[0-9]*C/g, "");
		data = data.replace(/\[[0-9]*;/g, "");
		//data = data.replace(/\[[0-9];[0-9][a-z]/g, "");
		//data = data.replace(/\[[0-9];[0-9][0-9]/g, "");
		//data = data.replace(/\[[0-9];[0-9][0-9][a-zA-Z]/g, "");
		data = data.replace(/\^/g, "");
		//data = data.replace(/^[a-zA-Z]/g, "");
		data = data.replace(/\[[0-9][0-9];[0-9][A-Z]/g, "\n");
//		data = data.replace(/\[0;7m[0-9]/g, "");
		data = data.replace(/\[[0-9][a-z]/g, "");
		data = data.replace(/\[[0-9][0-9][a-z]/g, "");
		data = data.replace(/\[[0-9][0-9][a-z]\([A-Z]/g, "");
		data = data.replace(/\[[0-9][0-9][A-Z][0-9]/g, "");
		data = data.replace(/\[[0-9][a-z]\[[0-9][A-Z]/g, "");
		data = data.replace(/\[[A-Z]\([A-Z][0-9]/g, "");
		data = data.replace(/\[[A-Z][0-9]\([A-Z][0-9]\[[0-9];[0-9][0-9]\([A-Z][0-9]/g, "");
		//data = data.replace(/\[[0-9][0-9][a-z]\([A-Z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]\[[0-9][0-9][a-z]/g, "");
		data = data.replace(/\[[0-9][A-Z]\[[0-9];[0-9][0-9][A-Z]\([A-Z][0-9][0-9]/g, "");
		data = data.replace(/\([A-Z]/g, "");
		data = data.replace(/\([0-9][A-Z]\[[0-9];[0-9][0-9][A-Z][0-9]/g, "");

		data = data.replace(/\[m/g, "");
		data = data.replace(/\[K/g, "");
		//data = data.replace(/\n[0-9]*\n/g, "\n");
		
		data = data.replace("[3M[1;74H1", "");
		data = data.replace("[3M[1;74H2", "");
    */
	/*	
		data = data.replace("[30d(B", "");
		data = data.replace("[2d[3M[1;89H(B3", "");
		data = data.replace("[31d", "");
		data = data.replace("[32d", "");
		data = data.replace("[33d", "");
		data = data.replace("[2d[3MI", "");
		data = data.replace("[34d", "");
		data = data.replace("[2d[3M[1;89H(B1", "");
		data = data.replace("[2d[3M[1;89H(B2", "");
		data = data.replace("[2d[3M[1;89H(B3", "");
		data = data.replace("[2d[3M[1;89H(B4", "");
		data = data.replace("[2d[7M[1;89H3", "");
		data = data.replace("(B", "");
		data = data.replace("[2d[5M(B", "");
		data = data.replace("[2d[5M(", "");
		data = data.replace("[2d[4M[1;9H(B", "");
		data = data.replace("[89G5", "");
		data = data.replace("[89G6", "");
		data = data.replace("[89G7", "");
		data = data.replace("[2d[3M[1;89H8", "");
		data = data.replace("[25d(B", "");
		data = data.replace("[2d[10M", "");
		data = data.replace("[1;73H10", "");
		data = data.replace("[2d[3M[1;89H9", "");
		data = data.replace("[30d", "");
		data = data.replace("[27d", "");
		data = data.replace("[28d", "");
		data = data.replace("[29d", "");
		data = data.replace("[2d[6M[1;89H10", "");
		data = data.replace("[2d[3M[1;90H2", "");
		data = data.replace("[2d[3M[1;90H3", "");
		data = data.replace("[2d[3M[1;90H4", "");
		data = data.replace("[2d[3M[1;90H5", "");
		data = data.replace("[90G7", "");
		data = data.replace("[2d[3M[1;90H1", "");
		data = data.replace("[2d[5M[1;89H1", "");
		data = data.replace("[2d[3M", "");
		data = data.replace("[2d[5M", "");
		data = data.replace("[2d[4M", "");
		data = data.replace("[2d[9M", "");
		data = data.replace("[2d[3M[1;89H7", "");
		data = data.replace("[2d[3M", "");
		data = data.replace("[2;30r[2;1H[10T[1;34r[1;2H", "");
		data = data.replace("[12d(B[13d[15d[16d[18d[19d[21d[22d[24d[25d", "");
		data = data.replace("[89G0", "");
		data = data.replace("[26d", "");
		data = data.replace("[89G3", "");
		data = data.replace("[89G1", "");
		data = data.replace("[89G2", "");
		data = data.replace("[89G4", "");
		data = data.replace("[89G5", "");
		data = data.replace("[89G6", "");
		data = data.replace("[89G7", "");
		data = data.replace("[89G8", "");
		data = data.replace("[2d[4M", "");
		data = data.replace("[1;9H", "");
		data = data.replace("[26d", "");
		data = data.replace("[90G1", "");
		data = data.replace("[90G2", "");
		data = data.replace("[90G3", "");
		data = data.replace("[90G4", "");
		data = data.replace("[90G5", "");
		data = data.replace("[90G6", "");
		data = data.replace("[90G7", "");
		data = data.replace("[90G8", "");
		data = data.replace("[90G9", "");
		data = data.replace("[2d[8M[1;90H4", "");
		data = data.replace("[1;89H5", "");
		data = data.replace("[1;89H4", "");
		data = data.replace("[1;89H3", "");
		data = data.replace("[1;89H2", "");
		data = data.replace("[1;89H1", "");
		data = data.replace("[1;89H6", "");
		data = data.replace("[1;89H7", "");
		data = data.replace("[1;89H8", "");
		data = data.replace("[1;89H9", "");
		data = data.replace("[1;90H5", "");
		data = data.replace("[1;90H4", "");
		data = data.replace("[1;90H3", "");
		data = data.replace("[1;90H2", "");
		data = data.replace("[1;90H1", "");
		data = data.replace("[1;90H6", "");
		data = data.replace("[1;90H7", "");
		data = data.replace("[1;90H8", "");
		data = data.replace("[1;90H9", "");
		data = data.replace("[1;85H2", "");
		data = data.replace("[1;85H3", "");
		data = data.replace("[1;95H1", "");
		*/
    
		return data;
}
