var irc         = require('irc');
var ink         = require('../colors');
var dbg         = require('../debuglayer');
var config_data = require('../main').config_data;
var ver         = require('../version');
var users       = require('../users');
var zork        = require('../zorkiolayer');

var language = require('../main').language;

var write_apache_style_log = require('../sysloglayer').write_apache_style_log;

var ircl       = require('../irclayer');

var eh = require('../main').mainEmitter;

var version, version_path, version_dat_file;

exports.run = function(nick,message) {

  var usrs = users.get(); // get current list of users

  var bot = ircl.getBot();

  // Decide what player is making the request
  if (usrs[nick] !== undefined) { // strict comparison (dont convert to same type)
      bot.say(nick, language.lang.default.playerfound);
  } else {// player is not found so add player and continue

    bot.say(nick, language.lang.default.playernotfound);

    // besure version is int before assigning
    // our values to our required args
    if (Number(message.substring(3))) {
      version = message.substring(3);
      version_path = ver.get_version_path(version);
      version_dat_file = ver.get_version_dat_file(version);
    }
    // send a signal to zork to spawn the game
    console.log(ink.wtag+' IRC LAYER: sending signal to zork to spawn game'.warn);

    //eh.emit('startzork', nick, version, version_path+'/'+version_dat_file);
    // starting zork will update the player list internally
    zork.startzork(nick, version, version_path+'/'+version_dat_file);

    bot.say(nick, language.lang.default.spawn);

    bot.say(nick, language.lang.default.playerlist + users.strList());
  }
}
