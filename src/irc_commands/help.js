var irc         = require('irc');
var ink         = require('../colors');
var dbg         = require('../debuglayer');
var players     = require('../main').players;
var help        = require('../helpers.js').help;
var quick_help  = require('../helpers.js').quick_help;

var write_apache_style_log = require('../sysloglayer').write_apache_style_log;

var ircl       = require('../irclayer');

exports.runhelp = function(nick, message) {

  var bot = ircl.getBot();

  if (Number(message.substring(5))) {
    try {
      bot.say(nick, help(message.substring(5)));
    } catch (ex)  {
      console.log("Error displaying help information".error );
     // write_apache_style_log("error", nick, mcplayer, __function, "500", __line, __line, "Error displaying help information");
    }
  } else {
    try {
      bot.say(nick, "To review the help pages type 'HELP [1-5]'\n");
    } catch (ex)  {
      console.log("Error displaying help information".error );
     // write_apache_style_log("error", nick, mcplayer, __function, "500", __line, __line, "Error displaying help information");
    }
  }
}

exports.runquickhelp = function(nick, message) {
  var bot = ircl.getBot();

  if (message != "help" && message.substring(0,5) != 'help ' && !Number(message.substring(5))) {
    bot.say(nick, quick_help(nick));
  }
}
