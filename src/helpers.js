// Exports
exports.about = about;
exports.quick_help = quick_help;
exports.help = help;

/*
 * About
 */
function about() {
	
	data = (
			"Zork Arcade - Blamovision\n" +
			"Developed by Fatal and Fetter\n"+
			"IRC: <irc.subproto.com>\n" +
			"GIT: <https://bitbucket.org/BlackSuitIT/zork-bot>\n" +
			"WIKI: <https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Home>\n" +
			"BUGS: <https://bitbucket.org/BlackSuitIT/zork-bot/issues>\n"
			);
	
	return data;
}


/*
 * Quick help for start of game
 */
function quick_help(nick) {
	
	data = ("Welcome, "+nick+". I'm the irz (internet relay zork) bot located here on the local service. " +
	"To get started you can type 'SZ [1-3]' to start a game in ZORK I, II or III. " +
	"The 'PC <MSG>' command sends player chat messages. " +
	"The 'PL <NICK>' command will list players current scene location. " +
	"For more help on game commands send 'help [1-5]'.");
	
	return data;
	
}

/*
 * Help borrowed from THCNET Zork.
 */
function help(section) {
	
	if (section == 1) {
		
		data = ("Useful commands:\n" +
		    "The 'SZ [1-3]' command will start a game in Zork I, II, or  III.\n" +
		     "The 'EZ' command will end the currently running game.\n" +
		     "The 'PC <MSG>' command sends player chat messages.\n" +
				"The 'PL <NICK>' command will list players current scene location.\n" +
				"The 'SUPERBRIEF' command suppresses printing of long room descriptions for all rooms.\n"+
				"The 'VERBOSE' command restores long descriptions.\n" +
				"The 'ABOUT' command prints information as to why this site is here.\n" +
				"The 'INVENTORY' command lists the objects in your possession.\n" +
				"The 'LOOK' command prints a description of your surroundings.\n" +
				"The 'SCORE' command prints your current score and ranking.\n" +
				"The 'DIAGNOSE' command reports on your injuries, if any.\n\n" +
				"Command abbreviations:\n"+
				"The 'INVENTORY' command may be abbreviated 'I'.\n" +
				"The 'LOOK' command may be abbreviated 'L'.\n");
		
		return data;
		
	} else if (section == 2) {
		
		data = ("Actions:\n\n" +
				"Actions are verbs, words that show action. Such as TAKE, PUT, DROP, etc.\n" +
				"Various forms of verbs or 'actions' can be used such as PICK UP, PUT DOWN" + 
				"GET, PULL etc..\n\n" +
				
				"Directions:\n\n" +
				"NORTH, SOUTH, UP, DOWN. Directions can also be used as abbreviations, like 'N', 'S', 'E', 'W'.\n" +
				"Other more obscure directions exist like LAND and CROSS  but are only appropriate in certain situations.\n\n" +
				
				"Objects:\n\n" +
				"Most objects have names and can be referenced by them."
				);
		
		return data;
		
	} else if (section == 3) {
		
		data = ("Adjectives:\n" +
				"Some adjectives are understood and required when there are " +
				"two objects which can be referenced with the same 'name' like DOORs and BUTTONs.\n" +
				
				"Prepositions:\n" +
				"It may be necessary in some cases to include prepositions, but " +
				"the parser attempts to handle cases which aren't ambiguousm" +
				"without. Thus 'GIVE CAR TO DEMON' will work, as will 'GIVE DEMON "  +
				"CAR'. 'GIVE CAR DEMON' probably won't do anything interesting. " +
				"When a preposition is used, it should be appropriate; 'GIVE CAR " +
				"WITH DEMON' won't parse. \n" +
				
				"Sentences:" +
				"The parser understands a reasonable number of syntactic constructions"
				);
		
		return data;
		
	} else if (section == 4) {
		
		data = ("Containment:\n" +
				"Some objects can contain other objects. Many such containers can " +
				"be opened and closed. The rest are always open. They may or may " +
				"not be transparent. For you to access (e.g., take) an object " +
				"which is in a container, the container must be open. For you " +
				"to see such an object, the container must be either open or " +
				"transparent. Containers have a capacity, and objects have sizes; " +
				"the number of objects which will fit therefore depends on their " +
				"sizes. You may put any object you have access to (it need not be " +
				"in your hands) into any other object. At some point, the program " +
				"will attempt to pick it up if you don't already have it, which " +
				"process may fail if you're carrying too much. Although containers " +
				"can contain other containers, the program doesn't access more than " +
				"one level down. "
				);
		
		return data;
		
	} else if (section == 5) {
		
		data = ("Fighting:\n" +
				"Occupants of the dungeon will, as a rule, fight back when " +
				"attacked. In some cases, they may attack even if unprovoked. " +
				"Useful verbs here are 'ATTACK <villain> WITH <weapon>', 'KILL', " +
				"etc. Knife-throwing may or may not be useful. You have a " +
				"fighting strength which varies with time. Being in a fight, " +
				"getting killed, and being injured all lower this strength. " +
				"Strength is regained with time. Thus, it is not a good idea to " +
				"fight someone immediately after being killed. Other details " +
				"should become apparent after a few melees or deaths. "
				);
		
		return data;
		
	} else {
		// If we got here the help don't exist
		data = ("Sorry but the help command you are looking for does not exist. Please try again.");
	}
	
}

