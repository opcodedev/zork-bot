[Home](https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Home) | [Change Log](https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Change Log) | [Commands](https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Commands)  | [Multiplayer](https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Multiplayer%20Updates%20and%20Details)

#Zork Arcade Bot v1.0.0

Zork Arcade Bot is dedicated to the members of SubProto.

irc.subproto.com 7000 - SSL ONLY!
 
Special Thanks to THCNET DOT NET
for an awesome ZORK PHP Client we could tap into
with our original bots which eventually lead us to 
our current version running DOS Zork with Dumb Frotz!

Developed by Fatal and Fetter

ShoutOuts to da'crew
Fauxtonic, the3percent, pingray, moxi, chisleu, jaybles,
professorblak, frank and all the rest of you.
 
Respect,


##Requirements

NodeJS, NPM, dFrotz, DOS Zork I, II, III. 
Notice: Expect is no longer a requirement.


###dFrotz Installation

https://github.com/DavidGriffith/frotz
For compiling, installing, and uninstalling dumb frotz, use "make dumb",
"make install_dumb", and "make uninstall_dumb".


###Modules

Use npm to install the following.
irc, minecraft-protocol, colors, express, socket.io, irc-colors (irc may/may not install these)


##Instructions

1. mkdir yourdirectory
2. cd yourdirectory
3. Download/Clone Project into directory (cd into directory if contents are not loaded into cwd)
4. npm install all required modules ($ npm install)
5. cd src/
6. Configure config.json ($ cp example-config.json config.json)
7. node main.js


##Install via NPM (Current NPM Release v1.0.0)
1. mkdir yourdirectory
2. cd yourdirectory
3. npm install zork-bot
4. cd node_modules/zork-bot
5. cd src/
6. Configure config.json ($ cp example-config.json config.json)
7. node main.js


#Minecraft Layer
Running the Minecraft layer requires a server of version 1.7.10 or a server developed with the minecraft-protocol.
NOTE: The Minecraft layer is under development. Do not use this module with out inspection and/or expectations of
bugs.


##Requirements
You will also need the following features and/or bukkit plugins: 

Teleport Player : /tp [Player] <x> <y> <z>


#More Information


##Wiki

Read more about Zork-Bot @ https://bitbucket.org/BlackSuitIT/zork-bot/wiki/Home


##Issue Tracking

Read more about bugs and issues @ https://bitbucket.org/BlackSuitIT/zork-bot/issues


#Operating Systems and Devices

Tested on Linux - Ubuntu 14.04+ (3.13.0-45-generic)

Tested on Intel Edison (3.10.17-poky-edison-ww42+) - http://www.emutexlabs.com/ubilinux


Notes: Should run on Windows but has not been tested yet.


#Zork Arcade Bot Contributors


##Developers

###opCode

opCode - opCode Development Team

opCode.DevTeam@gmail.com

Fatal and Fetter 

via SubProto!



##Contributors

the3percent, pingray